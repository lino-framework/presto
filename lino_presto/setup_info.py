# -*- coding: UTF-8 -*-
# Copyright 2015-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
# quick test: python setup.py test -s tests.PackagesTests

# Note that this module does not have a docstring because any global variable
# defined here will override the global namespace of modules that read it with
# execfile.

SETUP_INFO = dict(
    name='lino_presto',
    version='22.12.1',
    install_requires=['lino_xl'],
    tests_require=['bleach'],
    description="A Lino for managing services",
    license_files=['COPYING'],
    include_package_data=True,
    zip_safe=False,
    author='Rumma & Ko Ltd',
    author_email='info@lino-framework.org',
    url="https://gitlab.com/lino-framework/presto",
    #~ test_suite = 'lino.test_apps',
    test_suite='tests',
    classifiers="""\
  Programming Language :: Python
  Programming Language :: Python :: 3
  Development Status :: 4 - Beta
  Environment :: Web Environment
  Framework :: Django
  Intended Audience :: Developers
  Intended Audience :: System Administrators
  License :: OSI Approved :: GNU Affero General Public License v3
  Natural Language :: English
  Natural Language :: French
  Natural Language :: German
  Operating System :: OS Independent
  Topic :: Database :: Front-Ends
  Topic :: Home Automation
  Topic :: Office/Business""".splitlines())

SETUP_INFO.update(long_description="""\

Lino Presto is an application for managing services with physical on-site
presence of the workers.  Deployments can be either individually scheduled or
recurring based on contracts.  Integrated calendar and contacts. Automatically
generate invoices based on calendar entries.   Optional functionalities include
accounting (payments, purchases, general ledger, VAT declarations).

- Project homepage: http://presto.lino-framework.org

- Documentation: https://lino-framework.gitlab.io/presto/

- Commercial information: https://www.saffre-rumma.net

The name "Presto" originally comes from "prestations de service", the French
expression for service providement.  It also means "quick" in Italian.

""")

SETUP_INFO.update(packages=[
    str(n) for n in """
lino_presto
lino_presto.lib
lino_presto.lib.contacts
lino_presto.lib.contacts.fixtures
lino_presto.lib.cal
lino_presto.lib.cal.fixtures
lino_presto.lib.accounting
lino_presto.lib.accounting.fixtures
lino_presto.lib.orders
lino_presto.lib.presto
lino_presto.lib.presto.fixtures
lino_presto.lib.products
lino_presto.lib.trading
lino_presto.lib.trading.fixtures
lino_presto.lib.users
lino_presto.lib.users.fixtures
lino_presto.projects
lino_presto.projects.presto1
lino_presto.projects.presto1.settings
""".splitlines() if n
])

SETUP_INFO.update(include_package_data=True)
