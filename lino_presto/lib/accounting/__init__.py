# -*- coding: UTF-8 -*-
# Copyright 2019 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Extends :mod:`lino_xl.lib.accounting`:
Add JournalGroups "Orders"
Add Journal.room to detail layout.
"""
from lino_xl.lib.accounting import Plugin
