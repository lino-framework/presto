===========================
The ``lino_presto`` package
===========================




Lino Presto is an application for managing services with physical on-site
presence of the workers.  Deployments can be either individually scheduled or
recurring based on contracts.  Integrated calendar and contacts. Automatically
generate invoices based on calendar entries.   Optional functionalities include
accounting (payments, purchases, general ledger, VAT declarations).

- Project homepage: http://presto.lino-framework.org

- Documentation: https://lino-framework.gitlab.io/presto/

- Commercial information: https://www.saffre-rumma.net

The name "Presto" originally comes from "prestations de service", the French
expression for service providement.  It also means "quick" in Italian.


